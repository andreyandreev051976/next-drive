import { Module, DynamicModule, Provider, Type } from '@nestjs/common';
import { StorageService } from './storage.service';
import {
  StorageOptions,
  StorageAsyncOptions,
  StorageOptionsFactory,
  LocalDisk,
} from './interfaces';
import { map } from './provider.map';
import { isAbsolute, join, resolve } from 'path';
import {
  ServeStaticModule,
  ServeStaticModuleOptions,
} from '@nestjs/serve-static';

export const normalizeOptions = (options: StorageOptions) => {
  const disks = Object.keys(options.disks).reduce(
    (disks, diskName) => {
      const targetDisk = { ...options.disks[diskName] };
      disks[diskName] = targetDisk;
      if (targetDisk.driver !== 'local') {
        return disks;
      }

      if (isAbsolute(targetDisk.basePath)) {
        return disks;
      }

      const resolvedPath = resolve(process.cwd(), targetDisk.basePath);
      targetDisk.basePath = resolvedPath;
      targetDisk.baseUrl ??= diskName;
      return disks;
    },
    {} as StorageOptions['disks'],
  );

  return {
    ...options,
    disks,
  };
};

const pickLocalPublicDisks = (disks: StorageOptions['disks']) => {
  return Object.keys(disks).reduce((localDisks, diskName) => {
    const targetDisk = disks[diskName];
    if (targetDisk.driver === 'local' && targetDisk.public !== false) {
      localDisks.push({
        ...targetDisk,
        name: diskName,
      });
    }
    return localDisks;
  }, []) as unknown as (LocalDisk & { name: string })[];
};

@Module({
  providers: [],
  exports: [],
})
export class StorageModule {
  static register(options: StorageOptions): DynamicModule {
    const computedOptions = normalizeOptions(options);
    const localPublicDisks = pickLocalPublicDisks(computedOptions.disks);
    return {
      global: true,
      module: StorageModule,
      imports: [
        ServeStaticModule.forRoot(
          ...localPublicDisks.map<ServeStaticModuleOptions>((disk) => ({
            rootPath: disk.basePath,
            serveRoot: join('/', disk.name),
          })),
        ),
      ],
      providers: [
        StorageService,
        {
          provide: map.STORAGE_OPTIONS,
          useValue: computedOptions,
        },
      ],
    };
  }

  static registerAsync(options: StorageAsyncOptions): DynamicModule {
    return {
      global: true,
      module: StorageModule,
      providers: [this.createStorageOptionsProvider(options), StorageService],
    };
  }

  private static createStorageOptionsProvider(
    options: StorageAsyncOptions,
  ): Provider {
    if (options.useFactory) {
      return {
        provide: map.STORAGE_OPTIONS,
        useFactory: options.useFactory,
        inject: options.inject || [],
      };
    }

    const inject = [
      (options.useClass || options.useExisting) as Type<StorageOptions>,
    ];
    return {
      provide: map.STORAGE_OPTIONS,
      useFactory: async (optionsFactory: StorageOptionsFactory) =>
        await optionsFactory.createStorageOptions(),
      inject,
    };
  }
}
