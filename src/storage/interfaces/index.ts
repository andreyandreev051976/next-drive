export * from './storageDriver';
export * from './storageOptions';

export interface StorageDriver$PutOptions extends Record<PropertyKey, any> {}

export interface StorageDriver$FileMetadataResponse {
  path?: string;
  contentType?: string;
  contentLength?: number;
  lastModified?: Date;
}

export type StorageDriver$GetFileResponse = Buffer | null;

export interface StorageDriver$PutFileResponse {
  path?: string;
  url?: string;
}

export interface StorageDriver$RenameFileResponse {
  path?: string;
  url?: string;
}
