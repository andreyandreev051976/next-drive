import { ModuleMetadata, Type } from '@nestjs/common/interfaces';

type ExternalDriver = 's3';
type LocalDriver = 'local';

export interface ExternalDisk {
  driver: ExternalDriver;
  bucket: string;
  key: string;
  secret: string;
  region: string;
  profile?: string;
  fetchRemoteCredentials?: boolean;
}

export interface LocalDisk {
  driver: LocalDriver;
  basePath: string;
  baseUrl?: string;
  initialize?: boolean;
  public?: boolean;
}

export type DiskOptions = ExternalDisk | LocalDisk;

export interface StorageOptions {
  default?: string;
  disks: Record<string, DiskOptions>;
}

export interface StorageOptionsFactory {
  createStorageOptions(): Promise<StorageOptions> | StorageOptions;
}

export interface StorageAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  name?: string;
  useExisting?: Type<StorageOptions>;
  useClass?: Type<StorageOptions>;
  useFactory?: (...args: any[]) => Promise<StorageOptions> | StorageOptions;
  inject?: any[];
}
