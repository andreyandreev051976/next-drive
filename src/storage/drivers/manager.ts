import { StorageDriver } from 'src/storage/interfaces';
import { Local } from './local';

export class DriverManager {
  private readonly driverMap: { [key: string]: any } = {
    local: Local,
  };

  getDriver(disk: string, config: Record<string, any>): StorageDriver {
    const driver = this.driverMap[config.driver];
    return new driver(disk, config);
  }
}
