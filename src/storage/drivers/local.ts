import {
  LocalDisk,
  StorageDriver,
  StorageDriver$FileMetadataResponse,
  StorageDriver$PutFileResponse,
  StorageDriver$RenameFileResponse,
} from '../interfaces';
import { join } from 'path';
import * as fs from 'fs-extra';

export class Local implements StorageDriver {
  private basePath: string = '';
  constructor(
    private disk: string,
    private config: LocalDisk,
  ) {
    this.basePath = this.config.basePath;
    if (this.config.initialize) {
      fs.mkdirSync(this.basePath, { recursive: true });
    }
  }

  async put(
    filePath: string,
    fileContent: any,
  ): Promise<StorageDriver$PutFileResponse> {
    await fs.outputFile(join(this.basePath || '', filePath), fileContent);
    return {
      path: join(this.config.basePath || '', filePath),
      url: join(filePath),
    };
  }

  async get(filePath: string): Promise<Buffer> {
    const res = await fs.readFile(filePath);
    return res;
  }

  async meta(filePath: string): Promise<StorageDriver$FileMetadataResponse> {
    const path = join(this.basePath || '', filePath);
    const res = await fs.stat(path);
    return {
      path,
      contentLength: res.size,
      lastModified: res.mtime,
    };
  }

  signedUrl(filePath: string, expire = 10): string {
    return '';
  }

  async exists(filePath: string): Promise<boolean> {
    return fs.pathExists(join(this.basePath || '', filePath));
  }

  async missing(filePath: string): Promise<boolean> {
    return !(await this.exists(filePath));
  }

  url(fileName: string) {
    if (!this.config.hasOwnProperty('baseUrl')) {
      const filePath = join('public', fileName);
      return `${this.basePath}/${filePath}`;
    }
    return '';
  }

  async delete(filePath: string): Promise<boolean> {
    try {
      await fs.remove(join(this.basePath || '', filePath));
    } catch (e) {
      return false;
    }
    return true;
  }

  async copy(
    path: string,
    newPath: string,
  ): Promise<StorageDriver$RenameFileResponse> {
    await fs.copy(
      join(this.basePath || '', path),
      join(this.basePath || '', newPath),
      { overwrite: true },
    );
    return {
      path: join(this.basePath || '', newPath),
      url: this.url(newPath),
    };
  }

  async move(
    path: string,
    newPath: string,
  ): Promise<StorageDriver$RenameFileResponse> {
    await this.copy(path, newPath);
    await this.delete(path);
    return {
      path: join(this.basePath || '', newPath),
      url: this.url(newPath),
    };
  }

  getClient(): null {
    return null;
  }

  getConfig(): Record<string, any> {
    return this.config;
  }
}
