import { MIME_TYPES } from '../data/mime';

export const getMimeFromExtension = (fileName: string): string => {
  const fileSplit = fileName.split('.');
  const fileExtension = fileSplit[fileSplit.length - 1];

  for (const mimeType in MIME_TYPES) {
    const meta = MIME_TYPES[mimeType];
    if (meta.extensions && meta.extensions.includes(fileExtension)) {
      return meta.extensions[0];
    }
  }

  return '';
};
