export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      DB_USER: string;
      DB_PASSWORD: string;
      DB_PORT: string;
      DB_HOST: string;
      DB_NAME: string;

      PORT: string;

      NODE_ENV: string;
    }
  }
}
