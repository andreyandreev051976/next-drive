export {};

interface IService {
  port: number;
}

interface IDB {
  port: number;
  host: string;
  username: string;
  password: string;
  database: string;
}

interface IEnvironment {
  environment: Environment;
}

interface IJwt {
  secret: string;
}

declare global {
  namespace GlobalVariables {
    interface Variables {
      service: IService;
      db: IDB;
      environment: IEnvironment;
      jwt: IJwt;
    }
  }
}
