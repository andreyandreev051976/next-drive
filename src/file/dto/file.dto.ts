import { IsString } from 'class-validator';

export class FileDTO {
  @IsString()
  id: string;
}
