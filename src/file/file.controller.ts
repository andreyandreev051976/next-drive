import {
  Controller,
  Get,
  Post,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FileService } from './file.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';

@ApiTags('file')
@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post(':storage/upload')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  upload(@UploadedFile() file, @Param('storage') storage: string) {
    console.log(file);
    return this.fileService.create(file, storage);
  }

  @Get(':storage')
  findAll(@Param('storage') storage: string) {
    return this.fileService.findAll(storage);
  }

  @Get(':storage/:id')
  findOne(@Param('storage') storage: string, @Param('id') id: string) {
    return this.fileService.findOne(id, storage);
  }

  @Patch(':storage/:id')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  update(
    @UploadedFile() file: Express.Multer.File,
    @Param('id') id: string,
    @Param('storage') storage: string,
  ) {
    return this.fileService.update(id, storage, file);
  }

  @Delete(':storage/:id')
  remove(@Param('storage') storage: string, @Param('id') id: string) {
    return this.fileService.remove(id, storage);
  }

  // @Post(':storage/upload/chunk')
  // @ApiConsumes('multipart/form-data')
  // @UseInterceptors(FileInterceptor('file'))
  // uploadChunk(
  //   @UploadedFile() file,
  //   @Param('storage') storage: string,
  //   @Body('chunkData') chunkData: any,
  //   @Body('fileData') fileData: any,
  // ) {
  //   return this.fileService.loadChunk(
  //     file,
  //     storage,
  //     JSON.parse(chunkData),
  //     JSON.parse(fileData),
  //   );
  // }
}
