import { Folder } from 'src/folder/entities/folder.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class File {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'bigint',
  })
  size: number;

  @Column({
    type: 'text',
  })
  mime: string;

  @Column({
    type: 'text',
    default: '',
  })
  originalName: string;

  @Column({
    type: 'text',
  })
  path: string;

  @Column({
    type: 'text',
    default: '',
  })
  url: string;

  @Column({
    type: 'text',
    default: '',
  })
  storage: string;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt: Date;

  @ManyToOne(() => Folder, (folder) => folder.files, { onDelete: 'SET NULL' })
  folder: Folder | null;
}
