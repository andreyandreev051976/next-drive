import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { Storage } from 'src/storage';
import { FileRepository } from './file.repository';
import { v4 as uuidv4 } from 'uuid';
import { UploadChunkDTO } from 'src/chunker/dto/uploadChunk.dto';
import { Chunker } from 'src/chunker';
import { join } from 'path';
import { In } from 'typeorm';

@Injectable()
export class FileService {
  constructor(
    private readonly fileRepository: FileRepository,
    private readonly chunker: Chunker,
  ) {}
  async create(file: Express.Multer.File, storage: string) {
    const storageDriver = Storage.disk(storage);
    if (!storageDriver) {
      throw new BadRequestException('Invalid storage');
    }
    const { mimetype, originalname, size } = file;
    const { path, url } = await storageDriver.put(
      `${uuidv4()}-${originalname}`,
      file.buffer,
    );
    const fileInstance = this.fileRepository.create({
      mime: mimetype,
      size,
      path,
      url: join(storage, url),
      storage,
      originalName: originalname,
    });
    await this.fileRepository.insert(fileInstance);
    return fileInstance;
  }

  async loadChunk(
    file: Express.Multer.File,
    storage: string,
    chunkData: UploadChunkDTO,
    fileData: Pick<Express.Multer.File, 'originalname' | 'size' | 'mimetype'>,
  ) {
    const result = await this.chunker.uploadChunk(
      storage,
      file,
      chunkData,
      fileData,
    );
    if (result.type === 'chunk') {
      return { data: 'chunk loaded', type: 'chunk' };
    }
    const { data } = result;

    const { path, url } = data;
    const fileInstance = this.fileRepository.create({
      mime: fileData.mimetype,
      size: fileData.size,
      path,
      url,
      storage,
      originalName: fileData.originalname,
    });
    await this.fileRepository.insert(fileInstance);
    return { type: 'file', payload: fileInstance };
  }

  async findAll(storage: string) {
    return await this.fileRepository.find({ where: { storage } });
  }

  async findAllById(ids: string[]) {
    return await this.fileRepository.find({ where: { id: In(ids) } });
  }

  async findOne(id: string, storage: string) {
    return await this.fileRepository.findOne({ where: { id, storage } });
  }

  async update(id: string, storage: string, newFile: Express.Multer.File) {
    const storageDriver = Storage.disk(storage);
    if (!storageDriver) {
      throw new BadRequestException('Invalid storage');
    }
    const file = await this.fileRepository.findOneBy({ id });
    if (!file) {
      throw new BadRequestException(`There is no file with id ${id}`);
    }
    const deleted = await storageDriver.delete(file.path);
    if (!deleted) {
      throw new ConflictException('Cannot delete file');
    }
    const { mimetype, originalname, size } = newFile;
    const { path, url } = await storageDriver.put(
      `${uuidv4()}-${originalname}`,
      newFile.buffer,
    );
    await this.fileRepository.update(
      { id },
      {
        mime: mimetype,
        size,
        path,
        url,
        storage,
        originalName: originalname,
      },
    );
    return this.findOne(id, storage);
  }

  async remove(id: string, storage: string) {
    const storageDriver = Storage.disk(storage);
    if (!storageDriver) {
      throw new BadRequestException('Invalid storage');
    }
    const file = await this.fileRepository.findOneBy({ id });
    if (!file) {
      throw new BadRequestException(`There is no file with id ${id}`);
    }
    const deleted = await storageDriver.delete(file.path);
    if (!deleted) {
      throw new ConflictException('Cannot delete file');
    }
    await this.fileRepository.delete({ id });
    return `deleted`;
  }
}
