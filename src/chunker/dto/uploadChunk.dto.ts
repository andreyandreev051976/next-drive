import { IsNotEmpty, IsNumber } from 'class-validator';

export class UploadChunkDTO {
  @IsNotEmpty()
  @IsNumber()
  totalChunks: number;
  @IsNotEmpty()
  @IsNumber()
  currentChunk: number;
}
