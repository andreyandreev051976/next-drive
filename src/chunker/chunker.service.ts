import { BadRequestException, Injectable } from '@nestjs/common';
import { UploadChunkDTO } from './dto/uploadChunk.dto';
import { Storage, StorageDriver } from 'src/storage';
import { CHUNK_STORAGE } from 'src/shared/constants';
import { createReadStream, createWriteStream } from 'fs-extra';
import { pipeline } from 'stream';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class Chunker {
  async uploadChunk(
    storage: string,
    file: Express.Multer.File,
    chunkData: UploadChunkDTO,
    fileData: Pick<Express.Multer.File, 'originalname' | 'size' | 'mimetype'>,
  ) {
    const storageDriver = Storage.disk(storage);
    if (!storageDriver) {
      throw new BadRequestException('Invalid storage');
    }
    const chunkDriver = Storage.disk(CHUNK_STORAGE);
    const { totalChunks, currentChunk } = chunkData;
    const chunkFilename = `${fileData.originalname}.${currentChunk}`;
    if (totalChunks === currentChunk) {
      return this.processChunks(
        chunkDriver,
        storageDriver,
        totalChunks,
        fileData,
      );
    }
    chunkDriver.put(chunkFilename, file.buffer);
    return {
      type: 'chunk',
      loaded: true,
      data: chunkData.currentChunk,
    } as const;
  }
  async processChunks(
    chunkDriver: StorageDriver,
    storageDriver: StorageDriver,
    totalChunks: number,
    fileData: Pick<Express.Multer.File, 'originalname' | 'size' | 'mimetype'>,
  ) {
    const file = await storageDriver.put(fileData.originalname, '');
    const writer = createWriteStream(`${uuidv4()}-${file.path}`);
    for (let i = 0; i <= totalChunks; i++) {
      const chunkPath = `${fileData.originalname}.${i}`;
      const chunk = await chunkDriver.get(chunkPath);
      await pipeline(createReadStream(chunk), writer);
      await chunkDriver.delete(chunkPath);
    }

    return {
      type: 'file',
      loaded: true,
      data: file,
    } as const;
  }
}
