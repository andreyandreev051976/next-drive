import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FolderService } from './folder.service';
import { CreateFolderDto } from './dto/create-folder.dto';
import { UpdateFolderDto } from './dto/update-folder.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AddFileDTO } from './dto/add-file.dto';
import { RemoveFileDTO } from './dto/remove-file.dto';
import { AddFolderDTO } from './dto/add-folder.dto';
import { RemoveFolderDTO } from './dto/remove-folder.dto';

@ApiTags('folder')
@Controller('folder')
export class FolderController {
  constructor(private readonly folderService: FolderService) {}

  @Post()
  @ApiBody({ type: CreateFolderDto })
  create(@Body() createFolderDto: CreateFolderDto) {
    return this.folderService.create(createFolderDto);
  }

  @Get()
  findAll() {
    return this.folderService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.folderService.findOne(id);
  }

  @Patch(':id')
  @ApiBody({ type: UpdateFolderDto })
  update(@Param('id') id: string, @Body() updateFolderDto: UpdateFolderDto) {
    return this.folderService.update(id, updateFolderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.folderService.remove(id);
  }

  @Patch(':id/add-files')
  addFiles(@Param('id') id: string, @Body() files: AddFileDTO) {
    return this.folderService.addFiles(id, files.files);
  }

  @Patch(':id/remove-files')
  removeFiles(@Param('id') id: string, @Body() files: RemoveFileDTO) {
    return this.folderService.removeFiles(id, files.files);
  }

  @Patch(':id/add-folders')
  addFolders(@Param('id') id: string, @Body() folders: AddFolderDTO) {
    return this.folderService.addFolders(id, folders.folders);
  }

  @Patch(':id/remove-folders')
  removeFolders(@Param('id') id: string, @Body() folders: RemoveFolderDTO) {
    return this.folderService.removeFolders(id, folders.folders);
  }
}
