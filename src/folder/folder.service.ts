import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateFolderDto } from './dto/create-folder.dto';
import { UpdateFolderDto } from './dto/update-folder.dto';
import { FolderRepository } from './folder.repository';
import { File } from 'src/file/entities/file.entity';
import { FileService } from 'src/file/file.service';
import { FileRepository } from 'src/file/file.repository';
import { Folder } from './entities/folder.entity';
import { In } from 'typeorm';

@Injectable()
export class FolderService {
  constructor(
    private readonly folderRepository: FolderRepository,
    private readonly fileService: FileService,
    private readonly fileRepository: FileRepository,
  ) {}
  async create(createFolderDto: CreateFolderDto) {
    const folder = this.folderRepository.create(createFolderDto);
    await this.folderRepository.insert(folder);
    return this.folderRepository.findOne({ where: { id: folder.id } });
  }

  findAll() {
    return this.folderRepository.find();
  }

  async findOne(id: string) {
    const folder = await this.folderRepository.findOne({ where: { id } });
    if (!folder) {
      throw new NotFoundException();
    }
    const children = await this.folderRepository.find({
      where: { parent: { id } },
    });
    const files = await this.fileRepository.find({
      where: { folder: { id } },
    });
    folder.children = children;
    folder.files = files;
    return folder;
  }

  async update(id: string, updateFolderDto: UpdateFolderDto) {
    await this.folderRepository.update({ id }, updateFolderDto);
    return await this.findOne(id);
  }

  async remove(id: string) {
    await this.findOne(id);
    return this.folderRepository.delete({ id });
  }

  async addFiles(id: string, filesId: File['id'][]) {
    const files = await this.fileService.findAllById(filesId);
    const folderFiles = await this.fileRepository.find({
      where: { folder: { id } },
    });
    const fileMap = folderFiles.reduce((fm, file) => {
      fm[file.id] = 1;
      return fm;
    }, {});
    const toAdd = files.filter((f) => !fileMap[f.id]);
    const folder = await this.findOne(id);
    folder.files = [...folderFiles, ...toAdd];
    this.folderRepository.save(folder);
    return folder;
  }

  async removeFiles(id: string, filesId: File['id'][]) {
    const files = await this.fileService.findAllById(filesId);
    const folder = await this.findOne(id);
    const folderFiles = await this.fileRepository.find({
      where: { folder: { id } },
    });
    const fileMap = files.reduce((fm, file) => {
      fm[file.id] = 1;
      return fm;
    }, {});
    folder.files = folderFiles.filter((file) => !fileMap[file.id]);
    this.folderRepository.save(folder);
    return folder;
  }

  async addFolders(id: string, foldersId: Folder['id'][]) {
    const folder = await this.findOne(id);
    const folders = await this.folderRepository.find({
      where: { id: In(foldersId) },
    });
    for (const childFolder of folders) {
      if (childFolder.id !== id) {
        childFolder.parent = folder;
        await this.folderRepository.save(childFolder);
      }
    }
    return this.findOne(id);
  }

  async removeFolders(id: string, foldersId: Folder['id'][]) {
    const folders = await this.folderRepository.find({
      where: { id: In(foldersId) },
    });
    const folderFolders = await this.folderRepository.find({
      where: { parent: { id } },
    });
    const folderMap = folderFolders.reduce((fm, file) => {
      fm[file.id] = 1;
      return fm;
    }, {});
    const toRemove = folders.filter((f) => folderMap[f.id]);
    for (const childFolder of toRemove) {
      if (childFolder.id !== id) {
        childFolder.parent = null;
        await this.folderRepository.save(childFolder);
      }
    }
    return this.findOne(id);
  }
}
