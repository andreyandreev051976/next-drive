import { DataSource, Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { Folder } from './entities/folder.entity';

@Injectable()
export class FolderRepository extends Repository<Folder> {
  constructor(private readonly dataSource: DataSource) {
    super(Folder, dataSource.createEntityManager());
  }
}
