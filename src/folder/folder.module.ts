import { Module } from '@nestjs/common';
import { FolderService } from './folder.service';
import { FolderController } from './folder.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FolderRepository } from './folder.repository';
import { FileModule } from 'src/file/file.module';
import { FileService } from 'src/file/file.service';

@Module({
  imports: [TypeOrmModule.forFeature([FolderRepository]), FileModule],
  controllers: [FolderController],
  providers: [FolderRepository, FolderService, FileService],
})
export class FolderModule {}
