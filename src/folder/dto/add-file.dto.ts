import { FileDTO } from 'src/file/dto/file.dto';

export class AddFileDTO {
  files: FileDTO['id'][];
}
