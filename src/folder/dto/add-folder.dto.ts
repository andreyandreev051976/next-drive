import { FolderDTO } from './folder.dto';

export class AddFolderDTO {
  folders: FolderDTO['id'][];
}
