import { FileDTO } from 'src/file/dto/file.dto';

export class RemoveFileDTO {
  files: FileDTO['id'][];
}
