import { FolderDTO } from './folder.dto';

export class RemoveFolderDTO {
  folders: FolderDTO['id'][];
}
