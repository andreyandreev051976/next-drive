import { IsNotEmpty, IsString } from 'class-validator';

export class FolderDTO {
  @IsNotEmpty()
  @IsString()
  id: string;
  @IsNotEmpty()
  @IsString()
  name: string;
}
