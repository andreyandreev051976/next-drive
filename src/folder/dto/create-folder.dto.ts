import { OmitType } from '@nestjs/swagger';
import { FolderDTO } from './folder.dto';

export class CreateFolderDto extends OmitType(FolderDTO, ['id']) {}
