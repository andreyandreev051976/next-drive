import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class Variables {
  constructor(private readonly configService: ConfigService) {}

  get service(): GlobalVariables.Variables['service'] {
    return this.configService.get('service');
  }

  get db(): GlobalVariables.Variables['db'] {
    return this.configService.get('db');
  }

  get environment(): GlobalVariables.Variables['environment'] {
    return this.configService.get('environment');
  }

  get jwt(): GlobalVariables.Variables['jwt'] {
    return this.configService.get('jwt');
  }
}
