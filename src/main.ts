import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Variables } from './setup/variables/variables.service';
import setupSwagger from './setup/swagger/configuration';

const ORIGIN = [/^my-awesome-domain\.ru$/, /\.my-awesome-domain\.ru$/];

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  setupSwagger(app);

  const variableService = app.get(Variables);

  const port = variableService.service.port;

  app.enableCors({
    origin: ORIGIN,
  });
  app.setGlobalPrefix('/api');

  app.startAllMicroservices();
  await app.listen(port);
}
bootstrap();
