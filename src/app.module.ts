import { Module } from '@nestjs/common';

import { ConfigModule } from '@nestjs/config';
import setupConfiguration from './setup/configuration/configuration';
import { DatabaseModule } from './setup/database/database.module';
import { VariablesModule } from './setup/variables/variables.module';
import { FileModule } from './file/file.module';
import { StorageModule } from './storage';
import { CHUNK_STORAGE } from './shared/constants';
import { FolderModule } from './folder/folder.module';

@Module({
  imports: [
    ConfigModule.forRoot(setupConfiguration()),
    VariablesModule,
    StorageModule.register({
      default: 'shared',
      disks: {
        'next-task': {
          driver: 'local',
          basePath: './next-task',
          initialize: true,
        },
        shared: {
          driver: 'local',
          basePath: './shared',
          initialize: true,
        },
        [CHUNK_STORAGE]: {
          driver: 'local',
          basePath: './chunk',
          initialize: true,
          public: false,
        },
      },
    }),
    DatabaseModule,
    FileModule,
    FolderModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
