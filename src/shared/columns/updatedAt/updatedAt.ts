import { UpdateDateColumn } from 'typeorm';

export class UpdatedAt {
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt: Date;
}
