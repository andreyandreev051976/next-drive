import { IsDateString } from 'class-validator';

export abstract class UpdatedAtDto {
  @IsDateString()
  updatedAt: Date;
}
