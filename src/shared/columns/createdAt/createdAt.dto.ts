import { IsDateString } from 'class-validator';

export class CreatedAtDto {
  @IsDateString()
  createdAt: Date;
}
