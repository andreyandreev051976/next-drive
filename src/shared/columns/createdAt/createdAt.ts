import { CreateDateColumn } from 'typeorm';

export abstract class CreatedAt {
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;
}
